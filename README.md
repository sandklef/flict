<!--
SPDX-FileCopyrightText: 2020 Henrik Sandklef <hesa@sandklef.com>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# flict / FOSS License Compliance Tool

We are moving this project to [Vinland Technology](https://github.com/vinland-technology/flict)'s space over at
github.